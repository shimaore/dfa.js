# @shimaore/dfa.js [![status](https://gitlab.com/shimaore/dfa.js/badges/master/pipeline.svg)](https://gitlab.com/shimaore/dfa.js/commits/master) [![coverage](https://gitlab.com/shimaore/dfa.js/badges/cov/coverage.svg)](https://gitlab.com/shimaore/dfa.js/commits/cov)

A javascript class library that models NFA and DFA (Deterministic Finite Automaton).

Based on fxfan's [dfa.js](https://github.com/fxfan/dfa.js).

## How to use
See examples(-ω-): 
  https://github.com/fxfan/dfa.js/blob/master/examples/expr.js

## License
MIT
